﻿using UnityEngine;


public class Detecting : MonoBehaviour
{
  

    public Transform enemy;
    private void FixedUpdate()
    {
       
        Vector3 Distance = enemy.position - transform.position;
        Distance.y = 0;

        if (Distance.magnitude <= 2)
        {
            if (Vector3.Dot(Distance.normalized, transform.forward) >  Mathf.Cos(90 * 0.5f * Mathf.Deg2Rad))

            {
                Debug.Log("vous etes face a un obstacle");
            }
        }

    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
    
        UnityEditor.Handles.color = new Color(0.8f, 0, 0, 0.4f); 

        Vector3 rotatedForward = Quaternion.Euler(
            0,
            -90 * 0.5f,
            0) * transform.forward;

        UnityEditor.Handles.DrawSolidArc(
            transform.position,
            Vector3.up,
            rotatedForward,
            90,
            2);

    }
#endif
}